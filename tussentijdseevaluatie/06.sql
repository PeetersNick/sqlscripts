USE TussentijdseEvaluatie;
SELECT * FROM vogels
WHERE GewichtInGram > 50
ORDER BY GewichtInGram ASC, Soort ASC;