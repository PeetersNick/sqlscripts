USE TussentijdseEvaluatie;
SET SQL_SAFE_UPDATES = 0;
UPDATE boeken 
SET Commentaar = 'Geen vrolijk boek'
WHERE JaarVanUitgave >= 1939 AND JaarVanUitgave <= 1945;
SET SQL_SAFE_UPDATES = 1;