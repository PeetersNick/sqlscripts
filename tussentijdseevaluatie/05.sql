USE TussentijdseEvaluatie;
/*ALTER TABLE vogels CHANGE Klank Klank VARCHAR(3) CHAR SET utf8mb4;*/
SET sql_safe_updates = 0;
update vogels set klank = substring(klank 1, 3);
SET sql_safe_updates = 1;