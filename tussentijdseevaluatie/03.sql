USE TussentijdseEvaluatie;
CREATE TABLE Personen(
Voornaam VARCHAR(100) CHAR SET utf8mb4 NOT NULL,
Familienaam VARCHAR(100) CHAR SET utf8mb4 NOT NULL,
Geboortedatum DATE NOT NULL,
GSMnummer CHAR(10) NOT NULL
);