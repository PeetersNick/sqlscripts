USE `aptunes`;
DROP procedure IF EXISTS `DemonstrateHandlerOrder`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `DemonstrateHandlerOrder` ()
BEGIN
	DECLARE randNumber INT DEFAULT 0;
	DECLARE CONTINUE HANDLER FOR SQLSTATE '45002' 
    begin
    SELECT 'State 45002 opgevangen. Geen probleem.';
    end;
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
		BEGIN
        RESIGNAL SET MESSAGE_TEXT = 'Ik heb mijn best gedaan!';
        END;
	SET randNumber = FLOOR(RAND() * 3) + 1;
	IF randNumber = 1 THEN
	signal sqlstate '45001';
	ELSEIF randNumber = 2  THEN
	signal sqlstate '45002';
	ELSE
	signal sqlstate '45003';
	END IF;
  
   
END$$

DELIMITER ;