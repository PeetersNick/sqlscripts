USE Modernways;
SELECT Voornaam, Taken.Omschrijving
FROM Leden LEFT JOIN Taken
  ON Leden.Id = Taken.leden_Id
UNION ALL
SELECT Voornaam, Taken.Omschrijving
FROM Leden RIGHT JOIN Taken
  ON Leden.Id = Taken.leden_Id
WHERE
  Leden.Id IS NULL