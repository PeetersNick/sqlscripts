USE `aptunes`;
DROP procedure IF EXISTS `GetAlbumDuration`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `GetAlbumDuration` (IN Id INT, OUT totalDuration SMALLINT UNSIGNED)
BEGIN
  DECLARE ok INTEGER DEFAULT 0;
  DECLARE songDuration TINYINT UNSIGNED DEFAULT 0;

  DECLARE currentDuration CURSOR FOR SELECT Lengte FROM Liedjes WHERE (Albums_Id = Id);

  DECLARE CONTINUE HANDLER
  FOR NOT FOUND SET ok = 1;
  SET totalDuration = 0;
  OPEN currentDuration;

  getSongLength: LOOP
        FETCH currentDuration INTO songDuration;
    IF ok = 1
    THEN
	LEAVE getSongLength;
	END IF;
    SET totalDuration = totalDuration + songDuration;
    END LOOP getSongLength;

  CLOSE currentDuration;
END$$

DELIMITER ;