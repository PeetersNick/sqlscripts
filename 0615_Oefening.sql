USE Modernways;
ALTER VIEW auteursboeken
AS
SELECT CONCAT(personen.Voornaam,' ',personen.Familienaam) AS 'Auteur', boeken.titel, boeken.id AS 'Boeken_Id'
FROM publicaties
INNER JOIN personen ON personen.Id = publicaties.Personen_Id
INNER JOIN boeken ON boeken.id = publicaties.Boeken_Id
