USE `aptunes`;
DROP procedure IF EXISTS `DemonstrateHandlerOrder`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `DemonstrateHandlerOrder` ()
BEGIN
	DECLARE randNumber TINYINT DEFAULT 0;
	DECLARE CONTINUE HANDLER FOR SQLSTATE '45002' SELECT 'State 45002 opgevangen. Geen probleem.' Message; 
	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SELECT 'Een algemene fout opgevangen.' Message;
	SET randNumber = floor(rand() * 3) +1;
	IF randNumber = 1 THEN
	signal sqlstate '45001';
	ELSEIF randNumber = 2  THEN
	signal sqlstate '45002';
	ELSE
	signal sqlstate '45003';
	END IF;
  
   
END$$

DELIMITER ;