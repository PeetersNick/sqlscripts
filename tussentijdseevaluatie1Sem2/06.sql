USE Tussentijdseevaluatie1bdb;
SELECT concat(leden.voornaam, ' ', leden.Familienaam), coalesce(boeken_Id, 'is nog nooit door iemand uitgeleend') AS 'Volledige naam'
from ontleningen left join leden on leden.Id = Leden_Id
where Boeken_Id is null
union
select coalesce(titel, 'heeft nog nooit iets uitgeleend'), boeken.titel As 'Titel'
from ontleningen right join boeken on boeken.Id = Boeken_Id
where Leden_Id is null;
