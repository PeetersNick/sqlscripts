USE Tussentijdseevaluatie1bdb;
CREATE VIEW BoekenInfo
AS
SELECT titel, concat(auteurs.voornaam, ' ', auteurs.Familienaam) As 'Naam Autheur', uitgeverijen.Naam As 'naam uitgeverij'
from boeken
inner join auteurs on auteurs.id = boeken.Auteurs_Id
inner join uitgeverijen on uitgeverijen.id = boeken.Uitgeverijen_Id