USE Tussentijdseevaluatie1bdb;
Create VIEW OnpopulaireBoeken
AS
SELECT titel
FROM boeken
LEFT JOIN ontleningen
ON boeken.id = ontleningen.boeken_Id
WHERE ontleningen.leden_Id IS NULL