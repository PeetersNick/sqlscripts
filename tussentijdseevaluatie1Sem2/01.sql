USE Tussentijdseevaluatie1bdb;
SELECT boeken.titel, concat(leden.Voornaam, ' ',leden.Familienaam) AS 'Naam'
FROM ontleningen
INNER JOIN leden ON ontleningen.leden_ID = Leden.Id
INNER JOIN boeken On ontleningen.boeken_ID = boeken.Id