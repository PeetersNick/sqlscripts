USE `aptunes`;
DROP procedure IF EXISTS `DangerousInsertAlbumreleases`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `DangerousInsertAlbumreleases` ()
BEGIN
  DECLARE numberOfAlbums INT DEFAULT 0;
  DECLARE numberOfBands INT DEFAULT 0;
  DECLARE randomAlbumId1 INT DEFAULT 0;
  DECLARE randomAlbumId2 INT DEFAULT 0;
  DECLARE randomAlbumId3 INT DEFAULT 0;
  DECLARE randomBandId1 INT DEFAULT 0;
  DECLARE randomBandId2 INT DEFAULT 0;
  DECLARE randomBandId3 INT DEFAULT 0;
  DECLARE randomValue TINYINT DEFAULT 0;
  declare exit handler for sqlexception
  begin
	rollback;
    select 'Error: stored procdure is beëindigd en alle wijzigingen zijn ongedaan gemaakt.';
  end;
  SELECT count(*) INTO numberOfAlbums FROM Albums; 
  SELECT count(*) INTO numberOfBands FROM Bands;
  set randomAlbumId1 = floor(rand()*numberOfAlbums) +1;
  set randomAlbumId2 = floor(rand()*numberOfAlbums) +1;
  set randomAlbumId3 = floor(rand()*numberOfAlbums) +1;
  set randomBandId1 = floor(rand()*numberOfBands) +1;
  set randomBandId2 = floor(rand()*numberOfBands) +1;
  set randomBandId3 = floor(rand()*numberOfBands) +1;
  start transaction;
  insert into albumreleases(Bands_Id,Albums_Id)
  values(randomBandId1,randomAlbumId1),(randomBandId2,randomAlbumId2);
    set randomValue = floor(rand()*3) +1;
    if randomValue = 1 then
    signal sqlstate '45000';
    end if;
    insert into albumrealeases(Bands_Id,Albums_Id)
    values(randomBandId3,randomAlbumId3);
  commit;
END$$

DELIMITER ;