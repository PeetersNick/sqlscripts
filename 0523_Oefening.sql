USE Modernways;

set sql_safe_updates = 0;
UPDATE huisdieren
Set leeftijd = 9
WHERE (baasje = 'Bert') and soort = 'kat' or (baasje = 'Christiane') and soort = 'hond';
set sql_safe_updates =1;