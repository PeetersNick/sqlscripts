Use ModernWays;
Alter table	huisdieren add column Geluid varchar(20) char set utf8mb4;
set sql_safe_updates = 0;
update huisdieren
set	geluid = 'WAF!'
where soort = 'hond';
update huisdieren
set geluid = 'miauwww...'
where soort = 'kat';
set sql_safe_updates = 1;