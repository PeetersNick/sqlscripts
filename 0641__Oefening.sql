USE `aptunes`;
DROP procedure IF EXISTS `NumberOfGenres`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `NumberOfGenres` (OUT Aantal TINYINT)
BEGIN
    SELECT COUNT(*)
    INTO Aantal
    FROM genres;
END$$

DELIMITER ;