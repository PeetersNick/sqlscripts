USE ModernWays;
CREATE TABLE Student(
Studentennummer INT AUTO_INCREMENT PRIMARY KEY,
Voornaam VARCHAR(100) CHAR SET utf8mb4 NOT NULL,
Familienaam VARCHAR(100) CHAR SET utf8mb4 NOT NULL
);

CREATE TABLE Opleiding(
Naam VARCHAR(100) CHAR SET utf8mb4 PRIMARY KEY
);

CREATE	TABLE Vak(
Naam VARCHAR(100) CHAR SET utf8mb4 PRIMARY KEY
);

CREATE TABLE Lector(
Personeelsnummer INT AUTO_INCREMENT PRIMARY KEY,
Voornaam VARCHAR(100) CHAR SET utf8mb4 NOT NULL,
Familienaam VARCHAR(100) CHAR SET utf8mb4 NOT NULL
);

ALTER TABLE Student
ADD COLUMN Semester TINYINT UNSIGNED NOT NULL,
ADD COLUMN opleiding_Naam VARCHAR(100) CHAR SET utf8mb4 NOT NULL, 
ADD CONSTRAINT fk_Student_Opleiding FOREIGN KEY (Opleiding_Naam) REFERENCES opleiding(Naam);

CREATE TABLE LectorGeeftVak(
Lector_personeelsnummer INT NOT NULL,
CONSTRAINT fk_lectorGeeftVak_vak FOREIGN KEY (Lector_personeelsnummer) REFERENCES lector(personeelsnummer),
vak_naam VARCHAR(100) CHAR SET utf8mb4 NOT NULL,
CONSTRAINT fk_lectorGeeftVak_lector FOREIGN KEY (vak_naam) REFERENCES vak(naam));

CREATE TABLE Opleidingsonderdeel(
semester TINYINT UNSIGNED NOT NULL,
vak_naam VARCHAR(100) CHAR SET utf8mb4 NOT NULL,
opleiding_naam VARCHAR(100) CHAR SET utf8mb4 NOT NULL,
CONSTRAINT fk_opleidingsonderdeel_vak FOREIGN KEY (vak_naam) REFERENCES vak(naam),
CONSTRAINT fk_opleidingsonderdeel_opleiding FOREIGN KEY (opleiding_naam) REFERENCES opleiding(naam));


