-- creer een nieuwe tabel met de voornaam en familienaam van de auteurs
use ModernWays;
drop table if exists Personen;
create table Personen (
    Voornaam varchar(255) not null,
    Familienaam varchar(255) not null
);
-- namen uit de tabel Boeken overzetten naar de tabel Personen met een subquerie
insert into Personen (Voornaam, Familienaam)
   select distinct Voornaam, Familienaam from Boeken;

-- de andere kolommen toevoegen
alter table Personen add (
   Id int auto_increment primary key,
   AanspreekTitel varchar(30) null,
   Straat varchar(80) null,
   Huisnummer varchar (5) null,
   Stad varchar (50) null,
   Commentaar varchar (100) null,
   Biografie varchar(400) null);
   
-- foreign key toevoegen
alter table Boeken add Personen_Id int null;

-- De foreign key kolom in de tabel Boeken invullen 
SET SQL_SAFE_UPDATES = 0;
update Boeken cross join Personen
    set Boeken.Personen_Id = Personen.Id
where Boeken.Voornaam = Personen.Voornaam and
    Boeken.Familienaam = Personen.Familienaam;
SET SQL_SAFE_UPDATES = 1;

-- De foreign key tabel verplicht maken
alter table Boeken change Personen_Id Personen_Id int not null;

-- dubbele kolommen verwijderen uit te tabel van boeken
alter table Boeken drop column Voornaam,
    drop column Familienaam;
   
-- de constraint toevoegen
alter table Boeken add constraint fk_Boeken_Personen
   foreign key(Personen_Id) references Personen(Id);