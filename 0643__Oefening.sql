USE `aptunes`;
DROP procedure IF EXISTS `CreateAndReleaseAlbum`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `CreateAndReleaseAlbum` (IN titel VARCHAR(100), IN bands_Id INT)
BEGIN
  start transaction;
	insert into Albums (Titel) values (titel);
	insert into Albumreleases (Albums_Id, Bands_Id) values (LAST_INSERT_ID(),bands_Id);
  commit;
END$$

DELIMITER ;