USE `aptunes`;
DROP procedure IF EXISTS `MockAlbumReleasesLoop`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `MockAlbumReleasesLoop` (IN extraReleases INT)
BEGIN
   DECLARE counter INT DEFAULT 0;
   DECLARE success BOOL;
   mock: LOOP
   CALL MockAlbumReleaseWithSuccess(success);
   IF success = 1 Then
    set counter = counter + 1;
	END IF;
    IF counter >= extraReleases Then
    LEAVE mock;    
    END IF;
END LOOP;
END$$

DELIMITER ;