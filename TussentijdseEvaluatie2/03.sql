USE TussentijdseEvaluatie2;
SELECT Genre, Regisseur, SUM(Inkomsten)
FROM Films
WHERE Inkomsten >= 100000000
GROUP BY Genre;