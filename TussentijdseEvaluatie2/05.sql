USE TussentijdseEvaluatie2;
CREATE TABLE Recensies(
Naam VARCHAR(100) CHAR SET utf8mb4 NOT NULL,
Link VARCHAR(2083) CHAR SET utf8mb4 NOT NULL,
Cijfer TINYINT NOT NULL,
Film_id INT NOT Null,
CONSTRAINT fk_Recensies_Films FOREIGN KEY (Film_id) REFERENCES Films(id));