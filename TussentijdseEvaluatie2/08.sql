USE TussentijdseEvaluatie2;
SELECT Regisseur, Genre, AVG(Films.Sterren)
FROM Films
WHERE Jaar > 2000
GROUP BY Regisseur, Genre
HAVING AVG(Sterren) >= 4;