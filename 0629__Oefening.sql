use ModernWays;
select studenten_id as 'id'
from Studenten inner join Evaluaties
on studenten.Id = evaluaties.Studenten_Id
group by studenten.id
having avg(Cijfer) > (select avg(Cijfer) from Evaluaties);