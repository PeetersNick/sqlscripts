USE `aptunes`;
DROP procedure IF EXISTS `MockAlbumRelease`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `MockAlbumRelease`()
BEGIN
  DECLARE numberOfAlbums INT DEFAULT 0;
  DECLARE numberOfBands INT DEFAULT 0;
  DECLARE randomAlbumId INT DEFAULT 0;
  DECLARE randomBandId INT DEFAULT 0;
  SELECT COUNT(*) INTO numberOfAlbums FROM albums;
  SELECT COUNT(*) INTO numberOfBands FROM bands;
  Set randomAlbumId = FLOOR(RAND() * numberOfAlbums) + 1;
  Set  randomBandId = FLOOR(RAND() * numberOfBands) + 1;  
  IF(randomBandId,randomAlbumID) not in (select * from Albumreleases) THEN
   INSERT INTO albumreleases (Bands_Id,Albums_Id) values (randomBandId,randomAlbumID);
   END IF;
END$$

DELIMITER ;