USE `aptunes`;
DROP procedure IF EXISTS `CleanupOldMemberships`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `CleanupOldMemberships` (IN someDate DATE, OUT numberCleaned INT)
BEGIN
	START TRANSACTION;
		SELECT COUNT(*)
		INTO numberCleaned
		FROM lidmaatschappen
		WHERE Einddatum IS NOT NULL AND Einddatum < someDate;
        set sql_safe_updates = 0;
		DELETE FROM lidmaatschappen
		WHERE Einddatum IS NOT NULL AND Einddatum < someDate;
		set sql_safe_updates = 1;
    COMMIT;
END$$

DELIMITER ;