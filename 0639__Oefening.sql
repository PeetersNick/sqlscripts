Use ModernWays;
insert into Personen(Voornaam, Familienaam)
values('Jean-Paul', 'Satre');

insert into Boeken(Titel, Stad, Verschijningsdatum, Commentaar, Categorie, Personen_Id)
values('De Woorden', 'Antwerpen', '1962', 'Een zeer mooi boek', 'Roman',(select Id from Personen where Familienaam = 'Satre' and Voornaam = 'Jean-Paul'))