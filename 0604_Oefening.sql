USE Modernways;

CREATE TABLE Uitleningen(
begindatum  DATE NOT NULL,
einddatum DATE,
leden_Id INT NOT NULL,
CONSTRAINT fk_uitleningen_leden
FOREIGN KEY (leden_Id)
REFERENCES leden(Id),
boeken_Id INT NOT NULL,
CONSTRAINT fk_uitleningen_boeken
FOREIGN KEY (boeken_Id)
REFERENCES boeken(Id));